echo "Backup script for the Visit Kirtipur Wordpress instance"
echo "======================================================="
echo
echo "This is an automated shell script that will back up a Wordpress installation"
echo "that is based on the Visit Kirtipur website (available as an open-source project"
echo "at https://bitbucket.org/kirtipurmun/docker-prod)."
echo
echo "No user intervention is required, unless an error is reported in your task runner."
echo
echo "Steps taken:"
echo "  Database backup:"
echo "    1. Back up Wordpress database to \`./sql-backup/wordpress.sql\`"
echo "    2. Commit \`wordpress.sql\` to branch \`backups/$(date +%Y-%m-%d)\`"
echo "    3. Push branch \`backups/$(date +%Y-%m-%d)\` to origin server"
echo "    4. Merge branch \`backups/$(date +%Y-%m-%d)\` into \`master\` for \`sql-backup\` repo"
echo "    5. Push \`master\` branch to origin server for \`sql-backup\` repo"
echo "  Wordpress backup:"
echo "    6. Commit all files in \`./wp-content\` to branch \`backups/$(date +%Y-%m-%d)\`"
echo "    7. Push branch \`backups/$(date +%Y-%m-%d)\` to origin server for \`wp-content\` repo"
echo "    8. Merge branch \`backups/$(date +%Y-%m-%d)\` into \`master\` for \`wp-content\` repo"
echo "    9. Push \`master\` branch to origin server for \`wp-content\` repo"
echo "  Production state backup:"
echo "   10. Add new revs of \`wp-content\` and \`sql-backup\` to \`backups/$(date +%Y-%m-%d)\`"
echo "   11. Commit new \`wp-content\` and \`sql-backup\` revs to \`backups/$(date +%Y-%m-%d)\`"
echo "   12. Push branch \`backups/$(date +%Y-%m-%d)\` to origin server for \`docker-prod\` repo"
echo "   13. Merge branch \`backups/$(date +%Y-%m-%d)\` into \`master\` for \`docker-prod\` repo"
echo "   14. Push \`master\` branch to origin server for \`docker-prod\` repo"
echo
echo "Please note that git \`commit\` and \`push\` operations require git to be correctly"
echo "configured in the virtual machine hosting Visit Kirtipur. Ensure that these"
echo "properties are set for git:"
echo "  1. Git's \`user.email\` and \`user.name\` settings"
echo "     This may be done on a repository-level or at a global level. If not set,"
echo "     the \`git commit\` operations will fail"
echo "  2. SSH authenitcation for git (e.g. for BitBucket)"
echo "     For \`git push\` operations to successfully authenticate with the remote"
echo "     repository, your virtual machine must have a private key initialised with"
echo "     the corresponding public key authorised by your git remote origin host."
echo "     If you are using BitBucket, you may add the public key to your personal"
echo "     or team's account under Settings > Security > SSH keys."
echo "     Without SSH authorisation for git, the \`git push\` commands will fail."
echo 
echo "==============================================================================="
echo 
export SQL_BACKUP_VER="$(date +%Y-%m-%d)"
echo "STEP 1: Back up Wordpress database to \`./sql-backup/wordpress.sql\`"
pushd sql-backup
git checkout backups/$SQL_BACKUP_VER || git checkout -b backups/$SQL_BACKUP_VER
docker-compose run --rm wpcli db export --exclude_tables=$(docker-compose run --rm wpcli db tables 'wp_user*' --format=csv) - > wordpress.sql
echo
echo
echo "STEP 2. Commit \`wordpress.sql\` to branch \`backups/$(date +%Y-%m-%d)\`"
git add wordpress.sql
git commit -m "Backup $SQL_BACKUP_VER, run from backup.sh"
echo
echo
echo "STEP 3. Push branch \`backups/$(date +%Y-%m-%d)\` to origin server"
git push origin backups/$SQL_BACKUP_VER
git checkout master
echo
echo
echo "STEP 4. Merge branch \`backups/$(date +%Y-%m-%d)\` into \`master\` for \`sql-backup\` repo"
git merge --no-edit backups/$SQL_BACKUP_VER
echo
echo
echo "STEP 5. Push \`master\` branch to origin server for \`sql-backup\` repo"
git push origin master
popd
pushd wp-content
echo
echo
echo "STEP 6. Commit all files in \`./wp-content\` to branch \`backups/$(date +%Y-%m-%d)\`"
git checkout backups/$SQL_BACKUP_VER || git checkout -b backups/$SQL_BACKUP_VER
git add .
git commit -m "Backup $SQL_BACKUP_VER, run from backup.sh"
echo
echo
echo "STEP 7. Push branch \`backups/$(date +%Y-%m-%d)\` to origin server for \`wp-content\` repo"
git push origin backups/$SQL_BACKUP_VER
git checkout master
echo
echo
echo "STEP 8. Merge branch \`backups/$(date +%Y-%m-%d)\` into \`master\` for \`wp-content\` repo"
git merge --no-edit backups/$SQL_BACKUP_VER
echo
echo
echo "STEP 9. Push \`master\` branch to origin server for \`wp-content\` repo"
git push origin master
popd
git checkout backups/$SQL_BACKUP_VER || git checkout -b backups/$SQL_BACKUP_VER
echo
echo
echo "STEP 10. Add new revs of \`wp-content\` and \`sql-backup\` to \`backups/$(date +%Y-%m-%d)\`"
git add sql-backup wp-content
echo
echo
echo "STEP 11. Commit new \`wp-content\` and \`sql-backup\` revs to \`backups/$(date +%Y-%m-%d)\`"
git commit -m "Backup $SQL_BACKUP_VER, run from backup.sh"
echo
echo
echo "STEP 12. Push branch \`backups/$(date +%Y-%m-%d)\` to origin server for \`docker-prod\` repo"
git push origin backups/$SQL_BACKUP_VER
echo
echo
echo "STEP 13. Merge branch \`backups/$(date +%Y-%m-%d)\` into \`master\` for \`docker-prod\` repo"
git checkout master
git merge --no-edit backups/$SQL_BACKUP_VER
echo
echo
echo "STEP 14. Push \`master\` branch to origin server for \`docker-prod\` repo"
git push origin master
echo
echo
