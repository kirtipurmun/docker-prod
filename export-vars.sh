# This script takes a list of variables and exports them to the shell.
for var in "$@"
do
  echo "$var=\"$(printenv $var)\"" >> .env
done