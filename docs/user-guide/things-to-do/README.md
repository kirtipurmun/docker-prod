# Things to do
## Visit Kirtipur
The _Things to do_ section of Visit Kirtipur lists the attractions and events that tourists and visitors to Kirtipur may be interested in. Authoring and editing these attractions can be done using the Wordpress admin interface. You will need to log in to Wordpress to add and edit Things to do articles.
![Things to do page on Visit Kirtipur](../images/compressed/things-to-do.jpg)

## Adding a Things to do article
These instructions will step through the process of adding an attraction to the Things to do index page on Visit Kirtipur. When categorised as either `Nature`, `Culture`, or `Event` within Wordpress, a post will appear as a summary under the respective heading on the _Things to do_ index page, linking through to the full article.

![Post summaries under the Culture category on Things to do](../images/compressed/things-to-do-post-summaries.jpg)

One post categorised as either `Nature`, `Culture`, or `Event` in Wordpress represents one _Things to do_ article.

### 1. Create a new post
If you are logged in as admin, you will see a toolbar at the top of window where you can quickly access Wordpress's administration features from anywhere on the website. From the _New_ menu, select _Post_ to enter into the _Add New Post_ page.
![The New menu visible when logged in](../images/compressed/new-post-menu.jpg)
#### Add New Post page
The _Add New Post_ page will allow you to customise the basic properties of the article. To create a fully-featured Things to do article we'll need to set the following before adding content:

1. Post title
2. Page (SEO) title
3. Page meta-description
4. _Things to do_ category
5. Featured image
6. Post excerpt
7. Hero image

![The Add New Post page marked up with the properties we need to change](../images/compressed/add-new-post-sections-to-be-completed.jpg)

### 1.1. Post title
#### Where is it used?
This title will be displayed verbatim on the )_Things to do_ index page , and it is used whenever this post is referenced (e.g., when using the page title variable when constructing an SEO title). It should also be the same as the title on the detailed Things To Do.

![Post title used on the Things to Do page](../images/compressed/post-title-summary.jpg)
#### Where is it set?
Select the Add title field at the top of the Add New Post page to enter a title for our Things To Do item.
![Enter a title into the Add New Post page](../images/compressed/add-new-post-title.jpg)

### 1.2. Page (SEO) title
#### Where is it used?
The page title (known as _SEO title_ in Yoast SEO) is used by search engines when referencing the page. The page title (known as SEO title in Yoast SEO) is used by search engines when referencing the page. The SEO title is shown in the web browser tab and in search engine results. They help to get visitors to the website from search results so these are used to highlight the main attraction of the destination.

For the Visit Kirtipur website, these have been structured as `<post title> - <reason of interest> - <website name>` and should be 50-60 characters. These should appeal to visitors to Nepal and Kathmandu who may not have heard of Kirtipur.

E.g. Champa Devi Hill – Hiking Kathmandu - Visit Kirtipur

When a search engine lists your page, it will display as many characters of the page title as the device supports, without modification (usually 50-60). Being different from the post title allows articles to display concise titles in index pages such as Things to do, and advertise the page with more interesting titles in search engine.

![An example page title in search results](../images/compressed/post-page-title.jpg)

#### Where is it set?
The page title can be customised within the Yoast SEO configuration for the post. This will allow you to append additional information to the title without having it appear in the Things to do index.

You may edit the title of the page by expanding the Yoast SEO settings below the page body, and clicking the _Edit snippet_ button below the _Snippet Preview_.
![The Edit snippet button within the Yoast SEO configuration for this post](../images/compressed/post-title-edit-snippet.jpg)
Typing % in the _SEO title_ field will reveal a list of dynamic properties for the page title. It is good practice to use these dynamic properties to keep things synchronised between updates to post titles, separators, site titles, etc.
![Dynamic properties for SEO title](../images/compressed/seo-title-percent.jpg)

* Make it unique for every page
*	Make sure it is 50-60 characters
*	Use the structure `<page title> - <reason of interest> - Visit Kirtipur`
* Make it appealing to visitors to Nepal and Kathmandu who may not have heard of Kirtipur

### 1.3. Page meta-description
#### Where is it used?
The meta description is shown on the search engine results page (although sometimes other sections with keywords will be displayed) and it is also shown on Facebook, WhatsApp messages, etc. if a link to the page is listed. Think of the meta description as a mini advertisement for the specific page so each meta description should be unique and persuade people to click the link. These should also be detailed enough to appeal to visitors to Nepal and Kathmandu who may not have heard of Kirtipur.

The meta descriptions should be around 160 characters (maximum 300) and should include key words that people will search for.

![An example of how the meta-description of a page will display in search results](../images/compressed/add-post-meta-description.jpg)

#### Where is it set?
You may edit this field under the Page Title field. Like the SEO title field, dynamic properties are also supported in the meta description, and may be useful in when trying to maintain consistenct in spelling or convention when referencing an attraction.
![The Meta description field within the Yoast SEO configuration for this post](../images/compressed/add-new-post-edit-meta-description.jpg)

* Make sure it is around 160 characters (no more than 300)
* Use it to highlight the main reasons to visit the attraction/event
* Make sure they’re different for each page
* Think about what people might be searching for to do around Kathmandu and include that. E.g. “best food in Kathmandu”, “day hikes near Kathmandu”, etc.


### 1.4. _Things to do_ category
#### Where is it used?
There are a set number of categories on Visit Kirtipur, and the website uses categories to filter which articles to display on index pages, such as _Things to do_. Assigning a new post to a category will display it under the relevant heading on the _Things to do_ index page. Visit Kirtipur’s Things to do are in three categories:

* Culture: used for attractions related to religion, history, culture
*	Nature: used for attractions related to the outdoors, sports, views
*	Events: used for specific annual festivals and local events

For example, each of the articles displayed under the _Events_ heading on the _Things to do_ index page have been assigned to the "Events" category.
![Articles in the Events category appearing in the Events section of Things to do](../images/compressed/things-to-do-events-posts.jpg)

#### Where is it set?
The right-hand side panel of the _Add New Post_ page contains a lot of the metadata for the article, including the categories that the article can be attributed to. If the sidebar is not visible, you can reveal it by clicking the cog in the top-right corner of the page.
![The Category settings for a post in the right-hand-side menu](../images/compressed/add-new-post-categories.jpg)
When setting the categories for a post:

* Make sure that you uncheck the _Uncategorized_ category
* Leave the "Things to do" category unchecked
* You may assign the post to multiple categories (e.g., "Culture" & "Events"), which will cause it to appear twice on the _Things to do_ index page—once under each heading

### 1.5 Featured image
#### Where is it used?
The featured image selected is used for the thumbnail for this article on the _Things to do_ index page. Please note, that this is set independently of the hero image that covers the top of the actual article page.
![The featured image displayed on the Things to do index page](../images/compressed/post-featured-image-summary.jpg)

#### Where is it set?
You can upload or select the featured image by clicking "Set featured image" from the right-hand-side settings column on the _Add new post_ page.
![The "Set featured image" button on the right-hand-side settings menu of the Add new post page](../images/compressed/add-new-post-set-featured-image.jpg)

This will open the Wordpress Media Library, giving you the option to select an existing image from the media library, or upload a new one by selecting the "Upload Files" tab.

![The Featured Image chooser](../images/compressed/add-new-post-set-featured-image-chooser.jpg)
When choosing an image, Wordpress will automatically optimise the image resolution to suit its usage.

If you are uploading a new image, it's best to use the following image properties:

* **JPEG** format
* **Quality** 75-90%
* **Resolution** 2560 pixels wide (with whatever height to maintain aspect ratio)

### 1.6. Post excerpt
#### Where is it used?
Post excerpts appear under the post title on the _Things to do_ index page. This text will not be truncated automatically, so try to keep its length consistent with excerpts from other articles in Things to do. Where possible, use the meta description as the post excerpt. This is an appealing description of the article and will ensure the lengths are consistent.
![The post excerpt that is displayed on the Things to do index page](../images/compressed/post-excerpt-summary.jpg)

#### Where is it set?
There is a field to set the Excerpt in the right-hand-side settings column of the _Add new post_ page. If this settings column is not visible, click the cog icon in the top-right corner of the page.
![The Excerpt field in the Add New Post page, along with the cog toggling the right-hand-side post settings column](../images/compressed/add-new-post-excerpt.jpg)

### 1.7 Hero image
#### Where is it used?
The hero image features at the top of an article, covering the width of the browser. It is designed to capture the user's attention and must therefore be a high-quality image. Note that for mobile devices, the image will be cropped to portrait, so make sure the main feature of the image is centred and works both in landscape and portrait orientation.
![The hero image on an article page](../images/compressed/new-post-hero-image.jpg)

Generally, the hero image should have the following characteristics:

* **JPEG** format
* **Quality** 75-90%
* **Resolution** 2560 pixels wide (with whatever height to maintain aspect ratio)

#### Where is it set?
The process for setting the hero image is a little complicated, as customising this value for each post requires a series of steps to override how structure of each post is displayed on the page.

##### 1.7.1. Locate the _Post Options_ section
Within the _Add New Post_ page, Find the "Post options" section, and expand it if it is collapsed by clicking the triangle to the right.
![The expanded Post Options settings on the Add New Post page and the toggle icon on the right](../images/compressed/add-new-post-post-options.jpg)

##### 1.7.2. Title Bar Setting
Activate the Title Bar Setting tab within the Post Options.
![The Title Bar Setting tab within Post Options on the Add New Post page](../images/compressed/add-new-post-title-bar-setting.jpg)

##### 1.7.3. Override Display Title Bar Section
Set the "Display Title Bar Section" option to "Yes", revealing the settings for the Title Bar Section below.
![Setting the Display Title Bar Section to Yes in the Title Bar Setting within Post Options on the Add New Post page](../images/compressed/add-new-post-title-bar-section-yes.jpg)

##### 1.7.4. Select the Layout Preset
Scroll down to the _Layout Presets_ setting, and select the "Fullscreen title bar with dark overlay on background", four presets below the Theme Default.
![The desired layout preset for the Title Bar Setting within Post Options on the Add New Post page](../images/compressed/add-new-post-layout-preset.jpg)

##### 1.7.5. Toggle Advanced Setting
Continue scrolling down the Title Bar Setting section to the _Enable Advanced Setting_ toggle. Click the toggle to switch it **on**.
![Enabling advanced settings for the title bar section](../images/compressed/add-new-post-title-display-enable-advanced-setting.jpg)

##### 1.7.6. Switch off titles and breadcrumbs
Continue scrolling down the Title Bar Setting section and disable the _Display Titles_ and _Display Breadcrumb_ options, clicking each one to switch them **off**.
![Switch off titles and breadcrumbs in Title Bar Advanced Setting](../images/compressed/add-new-post-toggle-title-title-breadcrumbs-off.jpg)

##### 1.7.7. Clear overlay colour
Hero images are typically muted with a translucent overlay to increase the contrast of any copy that is superimposed on top, increasing legibility of any text placed on top. The design of articles on Visit Kirtipur doesnt feature any such text so we will need to remove the default overlay for our image.

Click the grey square beside "Overlay Color" to reveal the colour picker, then click the boxed "X" in the top-right corner to select no overlay. Finally, click "Apply" to choose this [lack of] colour.
![Selecting the overlay colour for our hero image from the Title Bar Setting section](../images/compressed/add-new-post-display-titles-clear-overlay.jpg)

##### 1.7.8. Enable Title Background
To enable a hero image for our post, toggle the Enable Title Background by clicking the toggle, switching it **on**.

![The Enable Background toggle within the Title Bar Setting section](../images/compressed/add-new-post-title-setting-enable-background.jpg)

##### 1.7.9. Select the hero image
Finally, we can choose the hero image we wish to use for the page by selecting the Background image ofr the Title Bar. Once the Title Background is enabled (step 1.7.8), scroll down to the _Background Image_ field and click the "Add Image" button.
![The Add Image button for the Background Image within the Title Bar Setting](../images/compressed/add-new-post-add-background-image.jpg)

### 2. Publish the post
To save the post and make it available in the _Things to do_ index page, click "Publish..." in the top-right corner of the Add New Post page.
![Publish button on the Add New Post page](../images/compressed/add-new-post-publish.jpg)

Wordpress will ask you to confirm if you would like to publish. Click "Publish" again and the new article will appear under the repsective category heading on the _Things to do_ page.
![Confirm the post publication by clicking "Publish" in the Add New Post page](../images/compressed/add-new-post-publish-confirm.jpg)

### 3. Activate Elementor
Page templates are used by Things to do articles to ensure consistency between each page. Visit Kirtipur uses a custom page-building plugin, [_Elementor_](https://elementor.com/), which we use to manage the content of each Things to do article. This will replace the default Wordpress content editor for the page.

Once published, a post in Wordpress will be accessible as a standalone article, complete with its own URL. From within the Add New Post page, after clicking "Publish", click the "View Post" link in the admin menu to see the page, and check that the hero image displays correctly.

![The View Post link from the Add New Post page once published](../images/compressed/add-new-post-view-post.jpg)

The page will not have any content, nor even a heading—these are elements that will be part of the template that we insert on the page.

Navigate back to the editing interface for this newly published post by clicking the "Edit Post" link in the admin menu above.

![The Edit Post link in the top menu](../images/compressed/add-new-post-edit-published-post.jpg)

Once the editing interface loads for the post, click "Edit with Elementor" to configure the body of the page to use Elementor.

![The Edit with Elementor button on the Edit Post page](../images/compressed/add-new-post-edit-with-elementor.jpg)

This will save any changes to the post and load the Elementor content editing interface.

![The Elementor editing interface for a post](../images/compressed/add-new-post-elementor-interface.jpg)

### 4. Insert a page template
The Elementor interface is split into the toolbar on the left-hand side, and a preview of the article being edited on the right-hand side.

To insert a page template, scroll down within the preview, and click the Add Template, indicated by a folder icon, within the box entitled "Drag widget here".

![The Add Template icon within Elementor](../images/compressed/add-new-post-elementor-add-template.jpg)

A modal window showing a list of popular third-party page templates will appear over the Elementor interface. From this window, click the "My Templates" tab to see the Things To Do template options to insert.

![My Templates witin the Elementor template modal window](../images/compressed/add-new-post-elementor-choose-template.jpg)

Choose a template from the list of available templates, and click the "Insert" button for the corresponding template.

![The insert button active within the My Templates list](../images/compressed/add-new-post-elementor-insert-template.jpg)

Elementor will prompt you to choose whether to import document settings from the chosen template. Click "Yes" to import them.

![Import Document Settings prompt in Elementor upon inserting a template](../images/compressed/add-new-post-elementor-import-document-settings.jpg)

The chosen template will be inserted into the page, along with content that belongs to the template. You may change this copy to suit the current article.

![Template inserted into page](../images/compressed/add-new-post-elementor-template-inserted.jpg)

### 5. Edit article content
The content that is inserted onto the page is from a different article, you can edit the page heading, body copy, information boxes, etc. by clicking on them in the page preview.

Selecting a feature to edit will change the Elementor toolbar on the left-hand side to show the properties for that particular element.

![Heading element selected in Elementor](../images/compressed/add-new-post-elementor-select-element.jpg)

For greater control, update the copy from within the relevant field **inside the toolbar** (instead of attempting to change it in situ on the page).

![Editing an element within Elementor](../images/compressed/add-new-post-elementor-update-element.jpg)

Select each element on the page and update them with the relevant copy for this article accordingly.

### 6. Update page
Finally, to save the article and its content, click the "Update" button in the bottom right of the left-hand-side.

![The Update button within Elementor](../images/compressed/add-new-post-elementor-update.jpg)
