# User Manual
## Visit Kirtipur
This is the user manual documentation for Visit Kirtipur. This manual is written for an audience of content editors who wish to configure the content of the website using the Wordpress dashboard. [Developer documentation](/src/master/README.md) is available in the root of this repository.

## Getting started & login
The Visit Kirtipur website has been installed at https://visitkirtipur.com, and access to author content and update the configuration is done via the Wordpress Admin page (https://visitkirtipur.com/wp-admin/). To begin, log in to Wordpress by visiting this URL and entering in the username and password supplied.

![The Wordpress login screen to access the admin pages](images/wp-admin-login.png)

## Admin dashboard
Logging in successfully will display a dashboard with updates from vendors of plugins that are used by the website, along with statistics and a list of the latest activity by other administrators who have logged into the site.

![An example of the Wordpress dashboard with some updates](images/wp-admin-home.png)

### Types of content
The Visit Kirtipur website uses two types of content: _Posts_ and _Pages_, which each have a specific usage for Visit Kirtipur.

#### Pages
In Wordpress, a _page_ is a standalone section of the Visit Kirtipur website, and is not considered to be periodical nor enumarable. They're used to display content that doesn't belong to a set, or in some cases, contain a set of enumerable content.
Vist Kirtipur uses _pages_ to construct its homepage, the About Kirtipur page, Things to do page, etc., however items that you might expect to find under Things to do will not be created as pages. 
To see a list of pages that feature on Visit Kirtipur, click _Pages_ in the left-hand side menu of the Admin dashboard.
![The list of pages in Wordpress](images/wp-admin-pages.png)

#### Posts
Wordpress uses a _post_ to hold articles of content that are part of a set, and usually periodical. For Visit Kirtipur, we have chosen to use posts as a way of publishing information dedicated to each attraction in the Things to do section, and each article of advice in the Plan Your Trip section. This decision to use posts to represent this content has been guided by the [Phlonx template](https://phlox.pro/) that Visit Kirtipur uses, allowing us to display lists of posts, filtered by category.
To view the list of Things to do attractions and Plan Your Trip articles you may navigate click the Posts item in the left-hand side menu of the Admin dashboard.
![The list of posts in Wordpress](images/wp-admin-posts.png)