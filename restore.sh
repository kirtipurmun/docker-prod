echo "Restore script for the Visit Kirtipur Wordpress instance"
echo "========================================================"
echo
echo "This is an automated shell script that will restore the database as well as the"
echo "`wp-content` directory for websites based on the Visit Kirtipur project (source"
echo "available at https://bitbucket.org/kirtipurmun/docker-prod)."
echo
echo "This script does NOT take a backup before making changes."
echo
echo "No user intervention is required, unless an error is reported in your task runner."
echo
echo "Please ensure that the following environment variables are correctly set, and are"
echo "available in a plaintext file called \`.env\` (see \`example.env\` for details).\`"
echo
echo "  Used by Docker Compose:"
echo "    * MYSQL_ROOT_PASSWORD"
echo "    * MYSQL_USER"
echo "    * MYSQL_PASSWORD"
echo "    * WORDPRESS_DB_USER"
echo "    * WORDPRESS_DB_PASSWORD"
echo "    * VMA_USERNAME"
echo
echo "Separately, this script will require the following enviornment variables to be"
echo "available in your current shell environment to install Wordpress core:"
echo
echo "    * WORDPRESS_ADMIN"
echo "    * WORDPRESS_ADMIN_PASSWORD"
echo "    * WORDPRESS_ADMIN_EMAIL"
echo "    * WORDPRESS_SITEURL"
echo
echo "Steps taken:"
echo "  1. Teardown Docker Compose stack, deleting termporary volumes."
echo "  2. Start Docker Compose services"
echo "  3. Wait 30s for MySQL readiness"
echo "  4. Install Wordpress Core"
echo "  5. Import database from \`./sql-backup/wordpress.sql\`"
echo "  6. Update Wordpress option site URL"
echo "  7. Update Wordpress option home URL"
echo "  8. Update permissions on \`./wp-content/\`"
echo
echo "==============================================================================="
echo
echo
echo "STEP 1. Teardown Docker Compose stack, deleting termporary volumes."
docker-compose down -v
echo
echo
echo "STEP 2. Start Docker Compose services"
docker-compose up -d --force-recreate --build db wordpress
echo
echo
echo "STEP 3. Wait 30s for MySQL readiness"
sleep 30
echo
echo
echo "STEP 4. Install Wordpress Core"
docker-compose run --rm wpcli core install --url="${WORDPRESS_SITEURL}" --title="Visit Kirtipur" --admin_user=${WORDPRESS_ADMIN} --admin_email=${WORDPRESS_ADMIN_EMAIL} --admin_password=${WORDPRESS_ADMIN_PASSWORD} --skip-email
echo
echo
echo "STEP 5. Import database from \`./sql-backup/wordpress.sql\`"
docker-compose run --rm wpcli db import - < ./sql-backup/wordpress.sql
echo
echo
echo "STEP 6. Update Wordpress option site URL"
docker-compose run --rm wpcli option update siteurl "${WORDPRESS_SITEURL}"
echo
echo
echo "STEP 7. Update Wordpress option home URL"
docker-compose run --rm wpcli option update home "${WORDPRESS_SITEURL}"
echo
echo
echo "STEP 8. Update permissions on \`./wp-content/\`"
chmod -R 777 ./wp-content/cache
chmod -R 777 ./wp-content/w3tc-config
docker-compose run --rm wordpress bash -c 'chown -R www-data:www-data /var/www/html/wp-content'
echo
echo
