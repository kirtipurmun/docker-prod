# Visit Kirtipur Website & Production Environment
## A municipality tourism website
This repository contains Kirtipur Municipality's _Visit Kirtipur_ Docker configuration. _Visit Kirtipur_ is a customised Wordpress installation that presents users with information about the municipality's attractions for tourists. This repository is open-source, allowing other municipalities to clone this setup and use it for their own tourism website.

## This repository (production Docker environment)
This repository hosts the Docker configuration for production hosting, and contains two git submodules: [`wp-content`](https://bitbucket.org/kirtipurmun/wp-content/), and [`sql-backup`](https://bitbucket.org/kirtipurmun/sql-backup/), each of which have separate repositories.

Here is a brief introduction to the structure of this repository:
```
+-- wp-content/        # The wp-content submodule of our
                       # Wordpress installation. Contains the themes,
                       # plugin files, uploads, etc. that we use for
                       # Visit Kirtipur.
+-- sql-backup/        # The sql-backup submdule containing an SQL
                       # dump of the Visit Kirtipur Wordpress site.
+-- Caddyfile          # An example configuration for Caddy (web
                       # server software) to act as a reverse proxy.
+-- backup.sh          # A script that can be run to create a backup
                       # of each submodule and push them to the git
                       # remote destination.
+-- docker-compose.yml # The Docker Compose configuration for
                       # production.
+-- export-vars.sh     # Used by our continuous integration agent
                       # to pump environment variables into the
                       # shell environment that will start our
                       # server.
+-- restore.sh         # A script that can be run to (re)start the
                       # Docker instances and create a fresh
                       # installation of Wordpress before migrating
                       # the relevant data for Visit Kirtipur.
+-- update-script.sh   # A combined installation and updating
                       # script, which bootstraps the configuration
                       # and hosting of our Wordpress instance. This
                       # is used by our continuous integration
                       # pipeline.
```

## Containerised server infrastructure for Visit Kirtipur
This repo exists to separate the configuration of Wordpress from the details of its hosting. All the configuration data that Visit Kirtipur uses to customise Wordpress is contained in the submodules [`wp-content`](https://bitbucket.org/kirtipurmun/wp-content/), and [`sql-backup`](https://bitbucket.org/kirtipurmun/sql-backup/).

We use Docker to containerise three long-running services and one utility service.
![The Docker architecture diagram for Visit Kirtipur](docs/user-guide/images/compressed/docker-configuration.png)

### Service: MySQL 5.7 (image `mysql:5.7`)
The `vkdb` container is used to host a MySQL instance that is isolated to the Docker application stack. It exposes the default MySQL port (3306) to the private network used by the application stack. Visit Kirtipur uses this MySQL container to host the Wordpress database. This image is [maintained by Oracle and the MySQL community](https://hub.docker.com/_/mysql).

### Service: Wordpress (image `wordpress:latest`)
The `vkwp` container runs a hosted Wordpress installation, with Apache and PHP configured to work with the latest Wordpress. Visit Kirtipur uses this container to create a new installation of Wordpress before applying our own customisations. This image is [maintained by the Wordpress](https://hub.docker.com/_/wordpress/).

### Service: Caddy (image `abiosoft/caddy`)
The `vkproxy` container runs Caddy, which is a lightweight general purpose web server for *nix systems. Visit Kirtipur uses Caddy to proxy requests to the `vkwp` container via its Docker Compose network, and send responses to the client. We've chosen to use Caddy due to its simple [LetsEncrypt ACME implementation](https://letsencrypt.org/how-it-works/).

### Utility service: WP CLI (image `wordpress:cli`)
Finally, we instantiate a `vkwpcli` container as needed so that we can run commands to create or modify our Wordpress installation. This container is typically down until invoked with using Docker, eg.: 
```
docker-compose run --rm wpcli --info
```
The [full breadth of Wordpress CLI commands](https://developer.wordpress.org/cli/commands/) are supported, and is configured to operate on the installation of Wordpress contained in the `vkwp` container. This container will be killed at the completion of the command execution and the output will be piped to `stdout`.

### Detailed configuration
See the `docker-compose.yml` file within this repo for detailed configuration of each service.

# Setup for Deployment
## 1. Prerequsites
Deploying Visit Kirtipur will require a few prerequisites in the form of application software, environment variables and files. These are listed below:

1. A server (a **physical or virtial machine**) with enough power to handle a Docker host, a containerised instance of Apache, PHP7, MySQL 5.7, and Caddy server.<sup>*</sup> This configuration has only been tested on recent versions of CentOS and Ubuntu, however Windows hosting should be possible.
2. **SSH access** configured with public key authentication enabled.
3. **Docker** and **Docker Compose**, with [Docker Compose version 3 syntax compatibility](https://docs.docker.com/compose/compose-file/compose-versioning/).
4. A recent version of **`git`** installed
5. Internet connectivity, to reach the **git origin** (BitBucket.org in our case)
6. **Ports 80 and 443 access**, for hosting a web service.
7. **An email address** that can be used to verify certification with [Let's Encrypt](letsencrypt.org).

<sup>*</sup> These applications need not be installed on your server as Docker will automatically fetch the appropriate versions during initialisation.

## 2. Set up your server machine
This will suppose that you're working on a virtual machine running some flavour of *nix (Linux, Unix, or MacOS).

### 2.1. Docker and Docker Compose
You may install Docker using a package manager, or by downloading the binaries yourself. Please see the [About Docker documentation on Docker Docs website](https://docs.docker.com/install/).

### 2.2. Git
Git comes pre-installed on many *nix distributions, however you may need to install it yourself if it's not available on the command line.

### 2.3. Git Configuration
Visit Kirtipur uses `git` to backup a snapshot of the database ([`sql-backup` repo](https://bitbucket.org/kirtipurmun/sql-backup/)) and `wp-content` directory ([`wp-content` repo](https://bitbucket.org/kirtipurmun/wp-content/)), pushing changes upstream to the Bitbucket.org hosted repositories on demand. For upstream changes to be pushed to a remote repository, the the git installation must be configured to work over SSH—which will require some configuration.

#### 2.3.1. Clone this `docker-prod` repository
Log in to the server you will be using to host Visit Kirtipur and navigate to your desired working directory. For most *nix installations, this will be the user directory (which you can enter by typing `cd ~` into your console).

Open the clone options modal within the BitBucket repository by clicking the "Clone" button on the repo homepage.
![Clone button on the repo homepage](docs/user-guide/images/compressed/clone-repo-clone-button.png)

Copy the clone command from the modal popup to your clipboard.
![Clone URI copied](docs/user-guide/images/compressed/clone-repo-copy-command.png)

Run this command from your working directory to clone the repository.
![Executing the git clone command in a terminal](docs/user-guide/images/compressed/clone-repo-terminal.png)

Once complete, you will have downloaded this repository into a new `./docker-prod` directory on your server.

#### Troubleshooting
If you have access issues ("Permision denied"), ensure that you have [set up Access Keys for BitBucket](https://confluence.atlassian.com/bitbucket/access-keys-294486051.html) so that you can authenticate with the BitBucket Servers.

#### 2.3.2. Populate submodules
Once you've cloned the repository, we will need to initialise and update the [`wp-content`](https://bitbucket.org/kirtipurmun/wp-content/), and [`sql-backup`](https://bitbucket.org/kirtipurmun/sql-backup/) submodules. To do this, enter the newlt created `docker-prod` directory:

```
$ cd docker-prod
```

And then run the `git` commands for initialising and updating sumbodules.

```
$ git submodule init
$ git submodule update
```

This will begin synchronising the [`wp-content`](https://bitbucket.org/kirtipurmun/wp-content/), and [`sql-backup`](https://bitbucket.org/kirtipurmun/sql-backup/) submodules, which is approximately 1GB (1008MB).

### 2.4 Configure environment
The Docker application stack (invoked by Docker Compose) requires a set of environment variables to be defined in order for MySQL, Wordpress, and Caddy containers to launch successfully. These **must be defined in a .env file** in the project root.

If you do not currently have a `.env` file in your project directory, create one now using a text editor (`vi`, `nano`, `pico`, etc.).

#### 2.4.1 MySQL container environment variables (`vkdb` image)
The MySQL container in our Docker application stack will require the following environment variables to be set in your `.env` file:

* `MYSQL_ROOT_PASSWORD` — Determines the root password for the MySQL installation.
* `MYSQL_USER` — Used to create a single user whose credentials will be used to connect to MySQL from Wordpress (PHP).
* `MYSQL_PASSWORD` — The password corresponding to the user determined in the `MYSQL_USER` variable above

#### 2.4.2 Wordpress & Wordpress CLI container environment variables (`vkwp` & `wpcli`)
The Wordpress installation uses the following environment variables to install a fresh copy of Wordpress upon deployment, using the following properties found in your `.env` file:

* `WORDPRESS_DB_USER` — The username that Wordpress will use to authenticate its MySQL connection. This **must** match the `MYSQL_USER` value above.
* `WORDPRESS_DB_PASSWORD` — The password corresponding to the `WORDPRESS_DB_USER` above. This **must** match the `MYSQL_PASSWORD` value above.
* `WORDPRESS_ADMIN` — The administrator account used for Wordpress to gain access to the content management system.
* `WORDPRESS_ADMIN_PASSWORD` — The password corresponding to the `WORDPRESS_ADMIN` username above.
* `WORDPRESS_ADMIN_EMAIL` — An email address associated with the account specified in `WORDPRESS_ADMIN` above.
* `WORDPRESS_SITEURL` — The URL of the website, complete with schema (and port if non-standard). No trailing slash. Wordpress uses a built-in redirect to this URL if accessed from any other origin.

#### Examples
Examples of each of the variables above can be found in the `example.env` file contained in this repo.

## 3. Run the Docker Compose stack
With a populated `.env` file, you should now be able to run the Docker Compose stack using the following command:
```
$ docker-compose up -d
```

The `vkdb`, `vkwp`, and `vkproxy` containers will launch and should persist if the environment is sucessfully configured. The `wpcli` container will also launch, however end abruptly, as it will require a working Wordpress installation to be present (and will not be until we have restored our backup).

#### Troubleshooting
The `docker-compose up -d` command starts Docker Compose in a background process (daemon), where logging and errors are not displayed immediately. To view the log and error output for the process of starting the Docker Compose stack, run the following command:

```
docker-compose logs -f --tail=50
```

This will track the output of all running containers for the Visit Kirtipur stack, going back to the last 50 lines of output (you may change this by modifying the `--tail=XX` flag, or omitting it entirely to see _all_ logging since the containers' inception).

## 4. Install Visit Kirtipur

If you navigate to your URL specified in `WORDPRESS_SITEURL`, you will be promopted to install Wordpress for the first time. If you proceed with installation, you will have an empty Wordpress site with plugins and media from Visit Kirtipur that you can configure from scratch. To re-instate Visit Kirtipur fully, we must run the `./restore.sh` shell script.

This will install a new copy of Wordpress, restore the MySQL database backup from `sql-backup/wordpress.sql`, and overwrite some of the Wordpress settings with the values we supplied in section 2.4.2 above.

### 4.1. Required environment variables
To run this restore, we will need to have the following environment variables set in your console environment (not just contained in the `.env` file):

* `WORDPRESS_ADMIN` — The administrator account used for Wordpress to gain access to the content management system.
* `WORDPRESS_ADMIN_PASSWORD` — The password corresponding to the `WORDPRESS_ADMIN` username above.
* `WORDPRESS_ADMIN_EMAIL` — An email address associated with the account specified in `WORDPRESS_ADMIN` above.
* `WORDPRESS_SITEURL` — The URL of the website, complete with schema (and port if non-standard). No trailing slash. Wordpress uses a built-in redirect to this URL if accessed from any other origin.

Export these from your `.env` file (this will export _all_ variables from your `.env` file) by running the following command:

```
$ export $(grep -v '^#' .env | xargs)
```

### 4.2. Run restore script
Once the required environment variables are in place, you may use the `./restore.sh` script to restore the latest production version of Visit Kirtipur. The restore script will execute the following:

1. Teardown Docker Compose stack, deleting termporary volumes
2. Start Docker Compose services
3. Wait 30s for MySQL readiness
4. Install Wordpress Core
5. Import database from `./sql-backup/wordpress.sql`
6. Update Wordpress option site URL
7. Update Wordpress option home URL
8. Update permissions on `./wp-content/`

Execute this restore script using the following command:

```
./restore.sh
```

## 5. Log in to Visit Kirtipur
Open your browser and navigate to the URL specified in your `WORDPRESS_SITEURL` environment variable. You should find that the connection is secure (thanks to automatic Let's Encrypt activation), and the home page should be identical to the [official Visit Kirtipur website](https://visitkirtipur.com).

You may access the Wordpress Admin login by appending `/wp-admin/` to your URL, and log in using the username and password you specified in the `WORDPRESS_ADMIN` and `WORDPRESS_ADMIN_PASSWORD` environment variables.

---

## Customising Visit Kirtipur for your own municipality
The instructions above will set up a version of Visit Kirtipur on a domain name specified by the environment variables noted above. The instance of Visit Kirtipur will be identical to the [official Visit Kirtipur website](https://visitkirtipur.com), so you will need to plan the following customisations:

### Colour Scheme
Visit Kirtipur uses a palette developed from the traditional Newari dress. You may use tools such as [Adobe Kuler's Colour Wheel](https://color.adobe.com/create/color-wheel/) to develop a colour palette that suits the aesthetic of your area.

Use the **Appearance > Customize** section of the Wordpress admin to update the global colour scheme for the website. The **Header > Header Section** settings will allow you to change the header background colour, however most of the colours used for text and headings are set individually within Elementor content blocks.

### Things to Do (Attractions) section
Each municipality has a unique set of attractions that help set it apart from everywhere else in Nepal. Make a list of attractions that tourists may wish to visit whilst travelling through your area. These may fit neatly into the Culture, Nature and Events sections of the Things to Do page.

You can read more about authoring Things to Do items in the [Things to Do documentation](/docs/user-guide/things-to-do/README.md).

### About Kirtipur section
The About Kirtipur page on Visit Kirtipur is used to give a general overview of the municipality and what it has to offer tourists. You may edit the contents of this page to suit your own municipality and the features that make it unique. To